class FeedController < ApplicationController

  def index(data_limit = params[:entries_limit])
    redirect_to url if session[:user].nil?
    @user = session[:user]
    @page = 'feed'

    usr = User.where(:login => @user).first

    @usr_tasks = Hash.new
    Task.where(:user_id => usr._id).each do |usr_task|
      @usr_tasks[usr_task._id.to_s] = usr_task
    end
    data_limit = 20 if data_limit.nil?
    @usr_data = Entry.where(:user_id => usr._id).desc(:created_at).limit(data_limit).to_a
    usr.new_entries = 0
    usr.save
  end

end
