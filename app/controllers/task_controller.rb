class TaskController < ApplicationController

  def index
    redirect_to "/" if session[:user].nil?
    @user = session[:user]
    user_id = User.where(:login => @user).first._id
    @user_tasks = Task.where(:user_id => user_id).to_a
  end

  def delete
    redirect_to "/" if session[:user].nil?
    task_id = params['task']
    task = Task.where(:_id => task_id).first
    xml = Unparsed.where(:_id => task.unparsed_id).first
    entries = Entry.where(:task_id => task_id)
    xml.delete
    task.delete
    entries.each do |entry|
      entry.delete
    end
    render :json => true
  end

  def upload
    redirect_to "/" if session[:user].nil?
    xml = params['task'].to_s
    Unparsed.new(:xml => xml).save
    render :json => true
  end

end