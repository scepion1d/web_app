class ApplicationController < ActionController::Base
  protect_from_forgery

  def sorted_by_timestamp?(array)
    (0 .. array.size-2).each do |i|
      if array[i].created_at > array[i+1].created_at
        return false
      end
    end
    true
  end

end
