class UserController < ApplicationController
  before_filter :authenticate , :except => [:sign_in, :sign_up, :sign_out]
  protect_from_forgery :except => [:sign_in, :sign_up, :sign_out]

  def index
    go_to
  end

  def sign_up
    login = params['login']
    pass = params['pass']
    email = params['email']
    if !login || !pass || !email
      redirect_to "/"
    end
    ok = false

    if User.where(:login => login).first.nil?
      if User.where(:email => email).first.nil?
        parameters = { :login => login, :password => pass, :email => email }
        user = User.create(parameters)
        session[:user] = user.login
        ok = true
      end
    end

    if ok
      render :json => true
    else
      render :json => false
    end
  end

  def sign_in
    login = params['login']
    pass = params['pass']
    if !login || !pass
      redirect_to "/"
    end

    user = User.where(:login => login, :password => pass).first
    if !user.nil?
      session[:user] = login
      render :json => true
    else
      render :json => false
    end


  end

  def sign_out
    session[:user] = nil
    render :json => true
  end

end