$(document).ready(
    function() {
        $(".post-body").hide();

        $(".post-head").click(
            function() {
                $(this).next(".post-body").slideToggle(600);
            }
        );
        var user = $("#user-name").html().replace(/ /g,"").replace(/\n/g,"");
        var pubnub = PUBNUB.init({
            publish_key   : 'pub-2ac280d8-894c-47d1-90db-a94f40bd55e5',
            subscribe_key : 'sub-d1e3ec0c-aa49-11e1-9ee9-058c2234feb4',
            security_key  : 'sec-N2FmZWRlMWQtY2JhYS00YzgzLThmZGUtNjkxYmE5OWViMTRm',
            origin        : 'pubsub.pubnub.com',
            ssl           : false
        });
        pubnub.ready();
        pubnub.subscribe({
            channel  : user,
            connect  : function() {
                console.log("connected to notifier for '" + user + "'");
            },
            callback : function(message) {
                $('#new-entries-btn').button("option", "label", "New entries: " + message);
                console.log(message);
            },
            error    : function(e) {
                console.log(e);
            }
        });
    }

);