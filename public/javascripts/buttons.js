$(document).ready(
    function () {
//  User Menu Buttons
//      Sign In Button
        $("#sign-in-btn").button().click(
            function () {
                $("#sign-in-form").dialog("open");
            }
        ).hover(
            function () {
                $(this).removeClass("highlight");
            },
            function () {
                $(this).addClass("highlight");
            }
        );

//      Sign Up Button
        $("#sign-up-btn").button().click(
            function () {
                $("#sign-up-form").dialog("open");
            }
        );

//      Sign Out Button
        $("#sign-out-btn").button().click(
            function () {
                $.ajax({
                    url: 'user/sign_out',
                    type:"POST",
                    success:function (data) {
                        if (data == true) {
                            window.location  = '/';
                        } else {
                            alert('Something gone wrong!')
                        }
                    }
                });
            });

//  Top Menu Buttons
//      Home Button
        $("#home-btn").button().click(
            function () {
                window.location = '/';
                $("#btn-text").removeClass("highlight");
            }
        );

//      Feed Button
        $("#feed-btn").button().click(
            function () {
                window.location = '/feed';
                $("#btn-text").removeClass("highlight");
            }
        );

//      Task Button
        $("#tasks-btn").button().click(
            function () {
                window.location = '/task';
                $("#btn-text").removeClass("highlight");
            }
        );

//      New entries button
        $("#new-entries-btn").button({
            icons: { secondary: "ui-icon-refresh" }
        }).click(
        );

        $("#upload-task-btn").button().click(
            function () {
                var xml = $("#task-edit-area").val();

                $( "#upload-confirm" ).dialog({
                    resizable: false,
                    height:160,
                    modal: true,
                    buttons: {
                        "Create": function() {
                            $( this ).dialog( "close" );
                            $.ajax({
                                url: "/task/upload",
                                type: "POST",
                                data: {
                                    task: xml
                                },
                                success: function(data){
                                    if (data == true) {
                                        $( "#success-dialog" ).dialog({
                                            modal: true,
                                            buttons: {
                                                Ok: function() {
                                                    $( this ).dialog( "close" );
                                                    window.setTimeout('location.reload()', 0);
                                                }
                                            }
                                        });
                                    } else {
                                        alert('Something gone wrong!')
                                    }
                                }
                            });
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
            }
        );

        $( ".task-title" )
            .button({
                disabled: true
            })
            .next()
            .button( {
                text: false,
                icons: { primary: "ui-icon-closethick" }
            })
            .click(function() {
                var task_id = $(this).attr("id");
                $( "#deletion-confirm" ).dialog({
                    resizable: false,
                    height:160,
                    modal: true,
                    buttons: {
                        "Delete": function() {
                            $( this ).dialog( "close" );
                            $.ajax({
                                url: "/task/delete",
                                type: "POST",
                                data: {
                                    task: task_id
                                },
                                success: function(data){
                                    if (data == true) {
                                        $( "#success-dialog" ).dialog({
                                            modal: true,
                                            buttons: {
                                                Ok: function() {
                                                    $( this ).dialog( "close" );
                                                    window.setTimeout('location.reload()', 0);
                                                }
                                            }
                                        });
                                    } else {
                                        alert('Something gone wrong!')
                                    }
                                }
                            });
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
            }).parent().buttonset();
    }
);