$(document).ready(function() {
    $("#dialog:ui-dialog").dialog("destroy");



    var allFields,

        login_min_length = 5,
        login_max_length = 50,
        login_regexp = /^[a-z]([0-9a-z])+$/i,

        pass_min_length = 6,
        pass_max_length = 10000,

        email_regexp = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;



    function class_timeout(o, c, time) {
        setTimeout(function() {
            o.removeClass(c, time);
        }, 50);
    }



    function updateTips(place, t) {
        var tips = $("#validateTips-" + place);
        tips.text(t).addClass("ui-state-error");
        class_timeout(tips, "ui-state-error", 1500);
    }



    function checkLength(place, o, value, min, max) {
        if(o.val().length > max || o.val().length < min) {
            o.addClass("ui-state-error");
            updateTips(place, "Length of " + value + " must be between " + min + " and " + max + ".");
            class_timeout(o, "ui-state-error", 1500);
            return false;
        } else {
            return true;
        }
    }



    function checkRegexp(place, o, regexp, n) {
        if(!(regexp.test(o.val()) )) {
            o.addClass("ui-state-error");
            updateTips(place, n);
            class_timeout(o, "ui-state-error", 1500);
            return false;
        } else {
            return true;
        }
    }



    $("#sign-in-form").dialog({
        autoOpen : false,
        height : 320,
        width : 400,
        modal : true,
        buttons : {
            "Sign in" : function() {
                var login = $("#login-sign-in"),
                    password = $("#pass-sign-in"),
                    bValid = true;

                allFields = $([]).add(login).add(password);
                allFields.removeClass("ui-state-error");

                bValid = bValid && checkLength(
                    "sign-in", login, "login",
                    login_min_length, login_max_length
                );

                bValid = bValid && checkLength(
                    "sign-in", password, "password",
                    pass_min_length, pass_max_length
                );

                bValid = bValid && checkRegexp(
                    "sign-in", login, login_regexp,
                    "Login may consist of a-z, 0-9, underscores, begin with a letter."
                );

                if(bValid) {
                    $( "#dialog:ui-dialog" ).dialog( "destroy" );
                    $.ajax({
                        url: $('#sign-in').attr('action'),
                        type: "POST",
                        data: {
                            login: login.val(),
                            pass: password.val()
                        },
                        success: function(data){
                            if (data == true) {
                                $( "#success-dialog" ).dialog({
                                    modal: true,
                                    buttons: {
                                        Ok: function() {
                                            $( this ).dialog( "close" );
                                            window.setTimeout('location.reload()', 0);
                                        }
                                    }
                                });
                            } else {
                                alert('Something gone wrong!')
                            }
                        }
                    });
                    $(this).dialog("close");
                }
            },
            "Cancel" : function() {
                $(this).dialog("close");
            }
        },
        close : function() {
            allFields.val("");
        }
    });



    $("#sign-up-form").dialog({
        autoOpen : false,
        height : 330,
        width : 350,
        modal : true,
        buttons : {
            "Sign up" : function() {
                var login = $("#login-sign-up"),
                    email = $("#email-sign-up"),
                    password = $("#pass-sign-up"),
                    bValid = true;

                allFields = $([]).add(login).add(email).add(password);
                allFields.removeClass("ui-state-error");

                bValid = bValid && checkLength(
                    "sign-up", login, "login",
                    login_min_length, login_max_length
                );

                bValid = bValid && checkLength(
                    "sign-up", email, "email",
                    6, 80
                );

                bValid = bValid && checkLength(
                    "sign-up", password,"password",
                    pass_min_length, pass_max_length
                );

                bValid = bValid && checkRegexp(
                    "sign-up", login, login_regexp,
                    "Login may consist of a-z, 0-9, underscores, begin with a letter."
                );

                bValid = bValid && checkRegexp("sign_up", email, email_regexp, "eg. ui@jquery.com");

                if(bValid) {
                    $( "#dialog:ui-dialog" ).dialog( "destroy" );
                    $.ajax({
                        url: $('#sign-up').attr('action'),
                        type: "POST",
                        data: {
                            login: login.val(),
                            pass: password.val(),
                            email: email.val()
                        },
                        success: function(data){
                            if (data == true) {
                                $( "#success-dialog" ).dialog({
                                    modal: true,
                                    buttons: {
                                        Ok: function() {
                                            $( this ).dialog( "close" );
                                            window.setTimeout('location.reload()', 0);
                                        }
                                    }
                                });
                            } else {
                                alert('Something gone wrong!')
                            }
                        }
                    });

                    $(this).dialog("close");
                }
            },
            "Cancel" : function() {
                $(this).dialog("close");
            }
        },
        close : function() {
            allFields.val("");
        }
    });

    $("#task-edit-area").resizable({
        handles: "se",
        minWidth: 649,
        maxWidth: 649,
        minHeight: 301
    }).val('\<?xml version="1.0" encoding="UTF-8"?>\n<mask>\n  \<title>Title of your task</title>\n\n  <author>Your login</author>\n\n  <link>task url</link>\n\n  <root>\n    \<tag class="" id=""></tag>\n  </root>\n\n  \<object name="" target="">\n    <tag class="" id=""></tag>\n  </object>\n</mask>');

});